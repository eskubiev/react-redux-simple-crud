/**
 *
 * SwitchInput
 *
 */

import React from 'react';

// components
import Input from 'components/Input';


class SwitchInput extends React.PureComponent {
  render() {
    const {
      editMode,
      label,
      value,
      errorText,
      onChange,
    } = this.props;
    const labelPart = label
      ? (
        <label>{label}</label>
      )
      : ('');
    const inputPart = editMode
      ? (
        <Input
          value={value}
          errorText={errorText}
          onChange={onChange} />
      )
      : (
        <p>{value}</p>
      );
    return (
      <div className={`form-group ${errorText ? 'has-error' : ''}`}>
        {labelPart}
        {inputPart}
      </div>
    );
  }
}

SwitchInput.propTypes = {
  editMode: React.PropTypes.bool.isRequired,
  label: React.PropTypes.string,
  value: React.PropTypes.any,
  errorText: React.PropTypes.string,
  onChange: React.PropTypes.func.isRequired,
};

export default SwitchInput;