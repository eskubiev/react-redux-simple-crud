import styled from 'styled-components';

const Wrapper = styled.button`
  display: block;
  width: 100%;
  text-align: left;
`;

export default Wrapper;
