import React from 'react';
import Wrapper from './Wrapper';

class TableHeaderFilterButton extends React.PureComponent {
  render() {
    const {
      label,
      order,
      onClick,
    } = this.props;
    const iconClassName = !order
      ? 'glyphicon-sort' : order === 'asc'
        ? 'glyphicon-sort-by-attributes'
        : 'glyphicon-sort-by-attributes-alt';
    return (
      <Wrapper onClick={onClick}>
        {label} <span className={`glyphicon ${iconClassName}`}></span>
      </Wrapper>
    );
  }
}

TableHeaderFilterButton.propTypes = {
  label: React.PropTypes.string.isRequired,
  order: React.PropTypes.string,
  onClick: React.PropTypes.func,
};

export default TableHeaderFilterButton;
