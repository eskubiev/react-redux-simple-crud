/**
 *
 * PageTitle
 *
 */

import React from 'react';


class PageTitle extends React.PureComponent {
  render() {
    const {
      title,
    } = this.props;
    return (
      <div>
        <h1>
          {title}
        </h1>
      </div>
    );
  }
}

PageTitle.propTypes = {
  title: React.PropTypes.string.isRequired,
};

export default PageTitle;
