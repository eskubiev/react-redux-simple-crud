/**
 *
 * Input
 *
 */

import React from 'react';


class Input extends React.PureComponent {
  _handleChange(name, event) {
    const { onChange } = this.props;
    const value = event.target.value;
    onChange(name, value);
  }
  render() {
    const {
      value,
      errorText,
    } = this.props;
    const errorTextPart = errorText
      ? (
        <span className="help-block">{errorText}</span>
      )
      : ('');
    return (
      <div>
        <input
          type="text"
          className="form-control"
          value={value}
          onChange={this._handleChange.bind(this, 'name')} />
        {errorTextPart}
      </div>
    );
  }
}

Input.propTypes = {
  value: React.PropTypes.any.isRequired,
  errorText: React.PropTypes.string,
  onChange: React.PropTypes.func.isRequired,
};

export default Input;
