/**
*
* Table
*
*/

import * as _ from 'lodash';
import React from 'react';

// components
import TableRow from './row';


class Table extends React.PureComponent {
  render() {
    const {
      headers,
      body,
    } = this.props;
    const bodyPart = (
      _.map(body, (row, index) => {
        return (
          <TableRow key={index} rowData={row} />
        );
      })
    );
    return (
      <table className="table table-striped">
        <thead>
          <TableRow rowData={headers} />
        </thead>
        <tbody>
          {bodyPart}
        </tbody>
      </table>
    );
  }
}

Table.propTypes = {
  headers: React.PropTypes.array.isRequired,
  body: React.PropTypes.array.isRequired,
};

export default Table;
