/**
 *
 * TableRow
 *
 */

import * as _ from 'lodash';
import React from 'react';

// components
import TableCell from './cell';


class TableRow extends React.PureComponent {
  render() {
    const {
      rowData,
    } = this.props;
    const rowPart = (
      _.map(rowData, (cellData, index) => {
        return (
          <TableCell key={index} cellData={cellData} />
        );
      })
    );
    return (
      <tr>
        {rowPart}
      </tr>
    );
  }
}

TableRow.propTypes = {
  rowData: React.PropTypes.array.isRequired,
};

export default TableRow;
