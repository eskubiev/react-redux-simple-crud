/**
 *
 * TableCell
 *
 */

import React from 'react';

class TableCell extends React.PureComponent {
  render() {
    const {
      cellData,
    } = this.props;
    return (
      <td>
        {cellData}
      </td>
    );
  }
}

TableCell.propTypes = {
  cellData: React.PropTypes.any.isRequired,
};

export default TableCell;
