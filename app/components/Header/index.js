/**
 *
 * Header
 *
 */

import React from 'react';
import { Link } from 'react-router';


class Header extends React.PureComponent {
  render() {
    return (
      <nav className="navbar navbar-inverse">
        <div className="container">
          <ul className="nav navbar-nav navbar-right">
            <li><Link to="/">Домой</Link></li>
            <li><Link to="/employees">Список сотрудников</Link></li>
          </ul>
        </div>
      </nav>
    );
  }
}

export default Header;
