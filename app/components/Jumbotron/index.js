/**
 *
 * Jumbotron
 *
 */

import React, { PropTypes } from 'react';
import Button from 'components/Button';


class Jumbotron extends React.PureComponent {
  render() {
    const {
      header,
      leadText,
      buttonLabel,
      buttonOnClick,
    } = this.props;
    const headerPart = header
      ? (
        <h1>{header}</h1>
      )
      : ('');
    const leadTextPart = leadText
      ? (
        <p>{leadText}</p>
      )
      : ('');
    const buttonPart = buttonLabel && buttonOnClick
      ? (
        <Button
          type="primary"
          size="lg"
          onClick={buttonOnClick}>
          {buttonLabel}
        </Button>
      )
      : ('');
    return (
      <div className="jumbotron">
        {headerPart}
        {leadTextPart}
        <p>
          {buttonPart}
        </p>
      </div>
    );
  }
}

Jumbotron.propTypes = {
  header: React.PropTypes.string,
  leadText: React.PropTypes.string,
  buttonLabel: React.PropTypes.string,
  buttonOnClick: React.PropTypes.func,
};

export default Jumbotron;
