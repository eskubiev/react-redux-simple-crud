/**
 *
 * TableButton
 *
 */

import React, { PropTypes, Children } from 'react';
import Wrapper from './Wrapper';


class TableButton extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    const {
      children,
      disabled,
      onClick,
    } = this.props;
    return disabled
      ? (
        <button
          disabled={disabled}
          onClick={onClick}>
          {Children.toArray(children)}
        </button>
      )
      : (
        <Wrapper>
          <button
            disabled={disabled}
            onClick={onClick}>
            {Children.toArray(children)}
          </button>
        </Wrapper>
      );
  }
}

TableButton.propTypes = {
  children: React.PropTypes.node.isRequired,
  disabled: React.PropTypes.bool,
  onClick: React.PropTypes.func,
};

export default TableButton;
