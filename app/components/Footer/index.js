/**
 *
 * Footer
 *
 */

import React from 'react';
import Wrapper from './Wrapper';


class Footer extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div className="navbar-fixed-bottom row-fluid">
        <div className="navbar-in">
          <Wrapper>
            <div className="container text-center">
              Made by: Evgeny Skubiev (<a href="mailto:skubiev@gmail.com">skubiev@gmail.com</a>)
            </div>
          </Wrapper>
        </div>
      </div>
    );
  }
}

export default Footer;
