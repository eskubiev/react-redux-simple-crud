import styled from 'styled-components';

const Wrapper = styled.div`
  height: 40px;
  padding-top: 10px;
  background: #f9f9f9;
  border-top: 1px solid lightgrey
`;

export default Wrapper;
