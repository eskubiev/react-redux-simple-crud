/**
*
* Button
*
*/

import React, { PropTypes, Children } from 'react';


class Button extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    const {
      type,
      size,
      children,
      disabled,
      onClick,
    } = this.props;
    let btnTypeClass = 'btn-default';
    switch (type) {
      case 'primary':
        btnTypeClass = 'btn-primary';
        break;
      case 'danger':
        btnTypeClass = 'btn-danger';
        break;
      default:
        btnTypeClass = 'btn-default';
        break;
    }
    let btnSizeClass = '';
    switch (size) {
      case 'lg':
        btnSizeClass = 'btn-lg';
        break;
      case 'sm':
        btnSizeClass = 'btn-sm';
        break;
      case 'xs':
        btnSizeClass = 'btn-xs';
        break;
      default:
        btnSizeClass = '';
        break;
    }
    return (
      <button
        disabled={disabled}
        className={`btn ${btnTypeClass} ${btnSizeClass}`}
        onClick={onClick}>
        {Children.toArray(children)}
      </button>
    );
  }
}

Button.propTypes = {
  type: React.PropTypes.string,
  size: React.PropTypes.string,
  children: React.PropTypes.node.isRequired,
  disabled: React.PropTypes.bool,
  onClick: React.PropTypes.func,
};

export default Button;
