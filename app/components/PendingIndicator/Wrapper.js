import styled from 'styled-components';

const Wrapper = styled.div`
  background: #f9f9f9;
  position: fixed;
  width: 200px;
  top: 60px;
  right: 15px;
  padding-top: 20px;
  padding-bottom: 20px;
  text-align: center;
  border: 1px solid lightgrey;
  border-radius: 3px;
  opacity: .85;
`;

export default Wrapper;