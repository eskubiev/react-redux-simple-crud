import React from 'react';
import Wrapper from './Wrapper';


class PendingIndicator extends React.PureComponent {
  render() {
    const {
      isPending,
    } = this.props;
    return (
      <Wrapper className={isPending ? '' : 'hidden'}>
        Загрузка...
      </Wrapper>
    );
  }
}

PendingIndicator.propTypes = {
  isPending: React.PropTypes.bool,
};

export default PendingIndicator;
