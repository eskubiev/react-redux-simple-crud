/**
 *
 * EditableTable
 *
 */

import React from 'react';
import Table from 'components/Table';

class EditableTable extends React.Component {
  render() {
    const {
      tableConfig,
    } = this.props;
    const headers = tableConfig ? tableConfig.headers : [];
    const body = tableConfig ? tableConfig.body : [];
    return (
      <div>
        <Table headers={headers} body={body} />
      </div>
    );
  }
}

const tableConfigShape = {
  headers: React.PropTypes.array.isRequired,
  body: React.PropTypes.array.isRequired,
};

EditableTable.propTypes = {
  tableConfig: React.PropTypes.shape(tableConfigShape).isRequired,
};

export default EditableTable;
