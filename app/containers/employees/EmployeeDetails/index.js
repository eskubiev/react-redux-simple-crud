/**
 *
 * EmployeeDetails
 *
 */

import React from 'react';

// components
import PageTitle from 'components/PageTitle';
import EmployeeCard from 'containers/employees/EmployeeCard';
import PendingIndicator from 'components/PendingIndicator';


class EmployeeDetails extends React.PureComponent {
  componentDidMount() {
    const {
      actions,
      params,
    } = this.props;
    actions.employee.getEmployee(params.employeeId);
  }
  render() {
    const {
      data: {
        isPending,
        selectedEmployee,
      },
    } = this.props;
    return selectedEmployee && selectedEmployee.name
      ? (
        <div>
          <PendingIndicator isPending={isPending} />
          <PageTitle title={`Данные сотрудника ${selectedEmployee.name.first} ${selectedEmployee.name.last}`} />
          {React.cloneElement(<EmployeeCard employee={selectedEmployee} />, this.props)}
        </div>
      )
      : (
        <PendingIndicator isPending />
      );
  }
}

export default EmployeeDetails;
