/**
 *
 * EmployeeCreate
 *
 */

import React from 'react';

// components
import PageTitle from 'components/PageTitle';
import EmployeeCard from 'containers/employees/EmployeeCard';


class EmployeeCreate extends React.PureComponent {
  render() {
    return (
      <div>
        <PageTitle title="Создать сотрудника" />
        {React.cloneElement(<EmployeeCard editMode newEmployee />, this.props)}
      </div>
    );
  }
}

export default EmployeeCreate;
