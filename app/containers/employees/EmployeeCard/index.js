/**
 *
 * EmployeeCard
 *
 */

import React from 'react';
import { go } from '../../../routes';
import validate from 'validate.js';

// components
import Button from 'components/Button';
import SwitchInput from 'components/SwitchInput';

var constraints = {
  firstName: {
    presence: {
      message: 'Необходимо указать имя',
    },
  },
  lastName: {
    presence: {
      message: 'Необходимо указать фамилию',
    },
  },
  email: {
    presence: {
      message: 'Необходимо указать email'
    },
    email: {
      message: 'Не похоже, что это email'
    }
  },
  age: {
    presence: {
      message: 'Необходимо указать возраст',
    },
    numericality: {
      onlyInteger: true,
      greaterThan: 0,
      lessThan: 200,
      message: 'Возраст должен быть числом в разумных пределах (скажем, от 0 до 200 и без дробной части)',
    },
  },
};


class EmployeeCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = props
      ? this._prepareState(props)
      : {};
  }
  componentWillReceiveProps(nextProps) {
    this.setState(this._prepareState(nextProps));
  }
  _prepareState(props) {
    const employee = props.employee;
    return employee
      ? {
      editMode: props && props.editMode ? props.editMode : false,
      newEmployee: props ? props.newEmployee : false,
      employee: props
        ? {
        guid: props.employee.guid,
        firstName: props.employee.name.first,
        lastName: props.employee.name.last,
        email: props.employee.email,
        age: props.employee.age,
      }
        : {},
      validation: {
        hasErrors: false,
      },
    }
      : {
      editMode: props ? props.editMode : false,
      newEmployee: props ? props.newEmployee : false,
      employee: {},
      validation: {
        hasErrors: false,
      },
    };
  }
  _handleChange(name, value) {
    let { employee } = this.state;
    employee[name] = value;
    this.setState(employee);
  }
  _handleSaveEdit() {
    const {
      actions,
      newEmployee,
    } = this.props;
    const {
      employee,
    } = this.state;
    let { validation } = this.state;
    validation = validate(employee, constraints, { fullMessages: false });
    let hasErrors = false;
    _.forOwn(validation, (value) => {
      hasErrors = Boolean(value);
    });
    validation = _.assign({}, validation, {hasErrors});
    if (!validation.hasErrors) {
      const employeePrepared = {
        guid: employee.guid,
        email: employee.email,
        age: employee.age,
        name: {
          first: employee.firstName,
          last: employee.lastName,
        },
      };
      if (newEmployee) {
        actions.employee.create(employeePrepared);
      } else {
        actions.employee.update(employeePrepared);
      }
      this.setState({
        editMode: false,
      });
    } else {
      this.setState({
        validation,
      });
    }
  }
  _handleCancelEdit() {
    let newState = this._prepareState(this.props);
    newState.editMode = false;
    this.setState(newState);
  }
  _handleDelete() {
    const {
      actions,
      params,
    } = this.props;
    if (confirm('Вы уверены, что хотите удалить пользователя?')) {
      actions.employee.delete(params.employeeId);
    }
  }
  _handleSwitchToEdit() {
    this.setState({
      editMode: true,
    });
  }
  _handleKeyDown(event) {
    const {
      newEmployee,
      editMode,
    } = this.state;
    if (event.key === 'Enter') {
      this._handleSaveEdit();
    } else if (event.key === 'Escape' && !newEmployee && editMode) {
      this._handleCancelEdit();
    }
  }
  render() {
    const {
      data: {
        isPending,
      },
    } = this.props;
    const {
      editMode,
      newEmployee,
      employee,
      validation,
    } = this.state;
    const editButtonPart = !editMode
      ? (
      <Button
        disabled={isPending}
        onClick={this._handleSwitchToEdit.bind(this)}>
        Редактировать
      </Button>
    )
      : ('');
    const saveEditButtonPart = editMode
      ? (
      <Button
        disabled={isPending}
        type="primary"
        onClick={this._handleSaveEdit.bind(this)}>
        Сохранить
      </Button>
    )
      : ('');
    const cancelEditButtonPart = editMode && !newEmployee
      ? (
      <Button
        disabled={isPending}
        onClick={this._handleCancelEdit.bind(this)}>
        Отмена
      </Button>
    )
      : ('');
    const deleteButtonPart = !newEmployee
      ? (
      <Button
        disabled={isPending}
        type="danger"
        onClick={this._handleDelete.bind(this)}>
        Удалить
      </Button>
    )
      : ('');
    return (
      <div onKeyDown={this._handleKeyDown.bind(this)}>
        <SwitchInput
          editMode={editMode}
          label="First Name"
          value={employee && employee.firstName ? employee.firstName : ''}
          errorText={validation && validation.firstName ? validation.firstName[0] : null}
          onChange={(name, value) => this._handleChange('firstName', value)} />
        <SwitchInput
          editMode={editMode}
          label="Last Name"
          value={employee && employee.lastName ? employee.lastName : ''}
          errorText={validation && validation.lastName ? validation.lastName[0] : null}
          onChange={(name, value) => this._handleChange('lastName', value)} />
        <SwitchInput
          editMode={editMode}
          label="Email"
          value={employee && employee.email ? employee.email : ''}
          errorText={validation && validation.email ? validation.email[0] : null}
          onChange={(name, value) => this._handleChange('email', value)} />
        <SwitchInput
          editMode={editMode}
          label="Age"
          value={employee && employee.age ? employee.age : ''}
          errorText={validation && validation.age ? validation.age[0]: null}
          onChange={(name, value) => this._handleChange('age', value)} />
        {editButtonPart}
        {saveEditButtonPart}
        {cancelEditButtonPart}
        {deleteButtonPart}
      </div>
    );
  }
}

EmployeeCard.propTypes = {
  editMode: React.PropTypes.bool,
  newEmployee: React.PropTypes.bool,
  employee: React.PropTypes.object,
};

export default EmployeeCard;
