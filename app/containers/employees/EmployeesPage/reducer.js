import {
  GET_EMPLOYEES_REQUEST,
  GET_EMPLOYEES_SUCCESS,
  GET_EMPLOYEES_FAILURE,
  GET_EMPLOYEE_REQUEST,
  GET_EMPLOYEE_SUCCESS,
  GET_EMPLOYEE_FAILURE,
  CREATE_EMPLOYEE_REQUEST,
  CREATE_EMPLOYEE_SUCCESS,
  CREATE_EMPLOYEE_FAILURE,
  UPDATE_EMPLOYEE_REQUEST,
  UPDATE_EMPLOYEE_SUCCESS,
  UPDATE_EMPLOYEE_FAILURE,
  DELETE_EMPLOYEE_REQUEST,
  DELETE_EMPLOYEE_SUCCESS,
  DELETE_EMPLOYEE_FAILURE,
} from './constants';
import { fromJS, toJS } from 'immutable';
import * as _ from 'lodash';

function addEmployee(employee, employeeList) {
  if (employee) employeeList.push(employee);
  return employeeList;
}

function removeEmployee(employee, employeeList) {
  if (employee) _.remove(employeeList, {guid: employee.guid});
  return employeeList;
}

function updateEmployee(employee, employeeList) {
  if (employee) {
    const index = _.indexOf(_.map(employeeList, 'guid'), employee.guid);
    if (index > -1) employeeList.splice(index, 1, employee);
  }
  return employeeList;
}

export default (state = fromJS({
  isPending: false,
  items: [],
  selectedItem: {},
  error: null,
  message: null,
  newItemGuid: 0,
}), action) => {
  switch (action.type) {
    case GET_EMPLOYEES_REQUEST:
      return state
        .set('isPending', true)
        .set('error', null);
    case GET_EMPLOYEES_SUCCESS:
      return state
        .set('isPending', false)
        .set('items', action.employees);
    case GET_EMPLOYEES_FAILURE:
      return state
        .set('isPending', false)
        .set('error', action.error);
    case GET_EMPLOYEE_REQUEST:
      return state
        .set('isPending', true)
        .set('error', null);
    case GET_EMPLOYEE_SUCCESS:
      return state
        .set('isPending', false)
        .set('selectedItem', action.employee);
    case GET_EMPLOYEE_FAILURE:
      return state
        .set('isPending', false)
        .set('error', action.error);
    case CREATE_EMPLOYEE_REQUEST:
      action.employeeData = _.assign({}, action.employeeData, {
        guid: `new-employee-${state.toJS().newItemGuid}`,
      });
      return state
        .set('isPending', true)
        .set('error', null);
    case CREATE_EMPLOYEE_SUCCESS:
      return state
        .set('isPending', false)
        .set('items', addEmployee(action.response, state.toJS().items))
        .set('newItemGuid', state.toJS().newItemGuid + 1);
    case CREATE_EMPLOYEE_FAILURE:
      return state
        .set('isPending', false)
        .set('error', action.error);
    case UPDATE_EMPLOYEE_REQUEST:
      return state
        .set('isPending', true)
        .set('error', null)
        .set('message', null);
    case UPDATE_EMPLOYEE_SUCCESS:
      return state
        .set('isPending', false)
        .set('items', updateEmployee(action.response, state.toJS().items));
    case UPDATE_EMPLOYEE_FAILURE:
      return state
        .set('isPending', false)
        .set('error', action.error);
    case DELETE_EMPLOYEE_REQUEST:
      return state
        .set('isPending', true)
        .set('error', null)
        .set('message', null);
    case DELETE_EMPLOYEE_SUCCESS:
      return state
        .set('isPending', false)
        .set('items', removeEmployee(action.response, state.toJS().items));
    case DELETE_EMPLOYEE_FAILURE:
      return state
        .set('isPending', false)
        .set('error', action.error);
    default:
      return state;
  }
};
