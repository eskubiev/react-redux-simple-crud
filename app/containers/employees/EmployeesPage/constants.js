export const GET_EMPLOYEES_REQUEST = 'frontend/EmployeesPage/GET_EMPLOYEES_REQUEST';
export const GET_EMPLOYEES_SUCCESS = 'frontend/EmployeesPage/GET_EMPLOYEES_SUCCESS';
export const GET_EMPLOYEES_FAILURE = 'frontend/EmployeesPage/GET_EMPLOYEES_FAILURE';

export const GET_EMPLOYEE_REQUEST = 'frontend/EmployeesPage/GET_EMPLOYEE_REQUEST';
export const GET_EMPLOYEE_SUCCESS = 'frontend/EmployeesPage/GET_EMPLOYEE_SUCCESS';
export const GET_EMPLOYEE_FAILURE = 'frontend/EmployeesPage/GET_EMPLOYEE_FAILURE';

export const CREATE_EMPLOYEE_REQUEST = 'frontend/EmployeesPage/CREATE_EMPLOYEE_REQUEST';
export const CREATE_EMPLOYEE_SUCCESS = 'frontend/EmployeesPage/CREATE_EMPLOYEE_SUCCESS';
export const CREATE_EMPLOYEE_FAILURE = 'frontend/EmployeesPage/CREATE_EMPLOYEE_FAILURE';

export const UPDATE_EMPLOYEE_REQUEST = 'frontend/EmployeesPage/UPDATE_EMPLOYEE_REQUEST';
export const UPDATE_EMPLOYEE_SUCCESS = 'frontend/EmployeesPage/UPDATE_EMPLOYEE_SUCCESS';
export const UPDATE_EMPLOYEE_FAILURE = 'frontend/EmployeesPage/UPDATE_EMPLOYEE_FAILURE';

export const DELETE_EMPLOYEE_REQUEST = 'frontend/EmployeesPage/DELETE_EMPLOYEE_REQUEST';
export const DELETE_EMPLOYEE_SUCCESS = 'frontend/EmployeesPage/DELETE_EMPLOYEE_SUCCESS';
export const DELETE_EMPLOYEE_FAILURE = 'frontend/EmployeesPage/DELETE_EMPLOYEE_FAILURE';
