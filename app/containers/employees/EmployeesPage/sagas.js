import * as _ from 'lodash';
import {
  take,
  call,
  put,
  select,
  fork,
  cancel,
} from 'redux-saga/effects';
const fetch = require('isomorphic-fetch');
import { LOCATION_CHANGE, push } from 'react-router-redux';

import {
  GET_EMPLOYEES_REQUEST,
  GET_EMPLOYEE_REQUEST,
  CREATE_EMPLOYEE_REQUEST,
  UPDATE_EMPLOYEE_REQUEST,
  DELETE_EMPLOYEE_REQUEST,
} from './constants';
import * as Actions from './actions';

// Temp modules data
import * as Mates from './assets/mates.json';
const RESPONSE_TIMEOUT = 1500;
const delay = (ms) => new Promise(resolve => setTimeout(resolve, ms));


/**
 * This is example get all employees API call
 */
export function getEmployeesApi() {
  fetch('/api/employees', {
    method: 'GET',
    headers: new Headers({
      'Content-Type': 'application/json',
    }),
  }).then((response) => {
    return response.json()
      .then((data) => {
        return response.ok ? {
          response: data,
        } : {
          error: data.error,
        };
      });
  });
}

/**
 * This should be removed when proper API provided
 * @returns {{response}}
 */
export function getEmployeesApiTemp() {
  return {
    response: Mates,
  };
}

export function* getEmployees() {
  const {
    response,
    error,
  } = yield call(getEmployeesApiTemp);
  // This is to imitate call to API delay. Remove this when proper API added
  yield call(delay, RESPONSE_TIMEOUT);
  if (!error) {
    yield put(Actions.getEmployeesSuccess(response));
  } else {
    yield put(Actions.getEmployeesFailure(error));
  }
}

export function* getEmployeesRequest() {
  let runWatcher = true;
  while (runWatcher) {
    const action = yield take([GET_EMPLOYEES_REQUEST, LOCATION_CHANGE]);
    if (action.type === GET_EMPLOYEES_REQUEST) {
      yield call(getEmployees);
    } else {
      runWatcher = false;
    }
  }
}

/**
 * This is example get employee by id API call
 * @param employeeId
 * @returns {*}
 */
export function getEmployeeApi(employeeId) {
  return fetch(`/api/employees/${employeeId}`, {
    method: 'GET',
    headers: new Headers({
      'Content-Type': 'application/json',
    }),
  }).then(response => {
    return response.json()
      .then(data => {
        return response.ok ? {
          response: data,
        } : {
          error: data.error,
        };
      });
  });
}

/**
 * This should be removed when proper API provided
 * @param employeeId
 */
export function getEmployeeApiTemp(employeeId) {
  const employee = _.find(Mates, { guid: employeeId });
  return employee ? {
    response: employee,
  } : {
    error: 'Сотрудник не найден. Проверьте ваш запрос.',
  };
}

export function* getEmployee(employeeId) {
  const {
    response,
    error,
  } = yield call(getEmployeeApiTemp, employeeId);
  // This is to imitate call to API delay. Remove this when proper API added
  yield call(delay, RESPONSE_TIMEOUT);
  if (!error) {
    yield put(Actions.getEmployeeSuccess(response));
  } else {
    yield put(Actions.getEmployeeFailure(error));
  }
}

export function* getEmployeeRequest() {
  let runWatcher = true;
  while (runWatcher) {
    const action = yield take([GET_EMPLOYEE_REQUEST, LOCATION_CHANGE]);
    if (action.type === GET_EMPLOYEE_REQUEST) {
      yield call(getEmployee, action.employeeId);
    } else {
      runWatcher = false;
    }
  }
}

/**
 * This is example create employee API call
 * @param employeeData
 */
export function createEmployeeApi(employeeData) {
  return fetch('/api/employees', {
    method: 'POST',
    headers: new Headers({
      'Content-Type': 'application/json',
    }),
    body: JSON.stringify(employeeData),
  }).then(response => {
    return response.json()
      .then(data => {
        return response.ok ? {
          response: data,
        } : {
          error: data.error,
        };
      });
  });
}

/**
 * This should be removed when proper API provided
 * @param employeeData
 * @returns {{response: *}}
 */
export function createEmployeeApiTemp(employeeData) {
  return {
    response: employeeData,
  };
}

export function* createEmployee(employeeData) {
  const {
    response,
    error,
  } = yield call(createEmployeeApiTemp, employeeData);
  // This is to imitate call to API delay. Remove this when proper API added
  yield call(delay, RESPONSE_TIMEOUT);
  if (!error) {
    yield put(Actions.createEmployeeSuccess(response));
    yield put(push('/employees'));
  } else {
    yield put(Actions.createEmployeeFailure(error));
  }
}

export function* createEmployeeRequest() {
  let runWatcher = true;
  while (runWatcher) {
    const action = yield take([CREATE_EMPLOYEE_REQUEST, LOCATION_CHANGE]);
    if (action.type === CREATE_EMPLOYEE_REQUEST) {
      yield call(createEmployee, action.employeeData);
    } else {
      runWatcher = false;
    }
  }
}

/**
 * This is example update employee API call
 * @param employeeData
 * @returns {*}
 */
export function updateEmployeeApi(employeeData) {
  return fetch('/api/employees', {
    method: 'PUT',
    headers: new Headers({
      'Content-Type': 'application/json',
    }),
    body: JSON.stringify(employeeData),
  }).then(response => {
    return response.json()
      .then(data => {
        return response.ok ? {
          response: data,
        } : {
          error: data.error,
        };
      });
  });
}

export function updateEmployeeApiTemp(employeeData) {
  return {
    response: employeeData,
  };
}

export function* updateEmployee(employeeData) {
  const {
    response,
    error,
  } = yield call(updateEmployeeApiTemp, employeeData);
  // This is to imitate call to API delay. Remove this when proper API added
  yield call(delay, RESPONSE_TIMEOUT);
  if (!error) {
    yield put(Actions.updateEmployeeSuccess(response));
    yield call(getEmployee, response.guid);
  } else {
    yield put(Actions.updateEmployeeFailure(error));
  }
}

export function* updateEmployeeRequest() {
  let runWatcher = true;
  while (runWatcher) {
    const action = yield take([UPDATE_EMPLOYEE_REQUEST, LOCATION_CHANGE]);
    if (action.type === UPDATE_EMPLOYEE_REQUEST) {
      yield call(updateEmployee, action.employeeData);
    } else {
      runWatcher = false;
    }
  }
}

/**
 * This is example delete employee
 * @param employeeId
 */
export function deleteEmployeeApi(employeeId) {
  return fetch(`/api/employees/${employeeId}`, {
    method: 'DELETE',
    headers: new Headers({
      'Content-Type': 'application/json',
    }),
  }).then(response => {
    return response.json()
      .then(data => {
        return response.ok ? {
          response: data,
        } : {
          error: data.error,
        };
      });
  });
}

/**
 * This should be removed when proper API provided
 * @param employeeId
 */
export function deleteEmployeeApiTemp(employeeId) {
  const employee = _.find(Mates, { guid: employeeId });
  return employee ? {
    response: employee,
  } : {
    error: 'Сотрудник не найден. Проверьте ваш запрос.',
  };
}

export function* deleteEmployee(employeeId) {
  const {
    response,
    error,
  } = yield call(deleteEmployeeApiTemp, employeeId);
  // This is to imitate call to API delay. Remove this when proper API added
  yield call(delay, RESPONSE_TIMEOUT);
  if (!error) {
    yield put(Actions.deleteEmployeeSuccess(response));
    yield put(push('/employees'));
  } else {
    yield put(Actions.deleteEmployeeFailure(error));
  }
}

export function* deleteEmployeeRequest() {
  let runWatcher = true;
  while (runWatcher) {
    const action = yield take([DELETE_EMPLOYEE_REQUEST, LOCATION_CHANGE]);
    if (action.type === DELETE_EMPLOYEE_REQUEST) {
      yield call(deleteEmployee, action.employeeId);
    } else {
      runWatcher = false;
    }
  }
}

export default [
  getEmployeesRequest,
  getEmployeeRequest,
  createEmployeeRequest,
  updateEmployeeRequest,
  deleteEmployeeRequest,
];
