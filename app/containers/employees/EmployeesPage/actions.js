import {
  GET_EMPLOYEES_REQUEST,
  GET_EMPLOYEES_SUCCESS,
  GET_EMPLOYEES_FAILURE,
  GET_EMPLOYEE_REQUEST,
  GET_EMPLOYEE_SUCCESS,
  GET_EMPLOYEE_FAILURE,
  CREATE_EMPLOYEE_REQUEST,
  CREATE_EMPLOYEE_SUCCESS,
  CREATE_EMPLOYEE_FAILURE,
  UPDATE_EMPLOYEE_REQUEST,
  UPDATE_EMPLOYEE_SUCCESS,
  UPDATE_EMPLOYEE_FAILURE,
  DELETE_EMPLOYEE_REQUEST,
  DELETE_EMPLOYEE_SUCCESS,
  DELETE_EMPLOYEE_FAILURE,
} from './constants';

export function getEmployees() {
  return {
    type: GET_EMPLOYEES_REQUEST,
  };
}

export function getEmployeesSuccess(employees) {
  return {
    type: GET_EMPLOYEES_SUCCESS,
    employees,
  };
}

export function getEmployeesFailure(error) {
  return {
    type: GET_EMPLOYEES_FAILURE,
    error,
  };
}

export function getEmployee(employeeId) {
  return {
    type: GET_EMPLOYEE_REQUEST,
    employeeId,
  };
}

export function getEmployeeSuccess(employee) {
  return {
    type: GET_EMPLOYEE_SUCCESS,
    employee,
  };
}

export function getEmployeeFailure(error) {
  return {
    type: GET_EMPLOYEE_FAILURE,
    error,
  };
}

export function createEmployee(employeeData) {
  return {
    type: CREATE_EMPLOYEE_REQUEST,
    employeeData,
  };
}

export function createEmployeeSuccess(response) {
  return {
    type: CREATE_EMPLOYEE_SUCCESS,
    response,
  };
}

export function createEmployeeFailure(error) {
  return {
    type: CREATE_EMPLOYEE_FAILURE,
    error,
  };
}

export function updateEmployee(employeeData) {
  return {
    type: UPDATE_EMPLOYEE_REQUEST,
    employeeData,
  };
}

export function updateEmployeeSuccess(response) {
  return {
    type: UPDATE_EMPLOYEE_SUCCESS,
    response,
  };
}

export function updateEmployeeFailure(error) {
  return {
    type: UPDATE_EMPLOYEE_FAILURE,
    error,
  };
}

export function deleteEmployee(employeeId) {
  return {
    type: DELETE_EMPLOYEE_REQUEST,
    employeeId,
  };
}

export function deleteEmployeeSuccess(response) {
  return {
    type: DELETE_EMPLOYEE_SUCCESS,
    response,
  };
}

export function deleteEmployeeFailure(error) {
  return {
    type: DELETE_EMPLOYEE_FAILURE,
    error,
  };
}
