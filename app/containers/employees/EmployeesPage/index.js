/**
 *
 * EmployeesPage
 *
 */

import * as _ from 'lodash';
import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { toJS } from 'immutable';
import { go } from '../../../routes';

// components
import PageTitle from 'components/PageTitle';
import EditableTable from 'containers/EditableTable';
import Button from 'components/Button';
import TableButton from 'components/TableButton';
import TableHeaderFilterButton from 'components/TableHeaderFilterButton';
import PendingIndicator from 'components/PendingIndicator';

// actions
import {
  getEmployees,
  getEmployee,
  createEmployee,
  updateEmployee,
  deleteEmployee,
} from './actions';


export class EmployeesPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      filter: {
        field: '',
        incrementOrder: '',
      },
    };
  }
  componentDidMount() {
    const { actions: {
      init,
    } } = this.props;
    init();
  }
  _handleDeleteEmployee(employeeId) {
    const { actions } = this.props;
    if (employeeId && confirm('Вы уверены, что хотите удалить сотрудника?')) {
      actions.employee.delete(employeeId);
    }
  }
  _handleFilterChange(fieldNew) {
    if (fieldNew) {
      const { filter: {
        field,
        order,
      } } = this.state;
      const orderNew = order === 'asc' && field === fieldNew ? 'desc' : 'asc';
      this.setState({
        filter: {
          field: fieldNew,
          order: orderNew,
        },
      });
    }
  }
  // TODO: Add quick edit functionality
  render() {
    const {
      actions,
      data,
      data: {
        isPending,
        employees,
      },
      params,
      children,
    } = this.props;
    const childProps = {
      actions,
      data,
      params,
    };
    const {
      filter,
    } = this.state;
    const firstNameHeader = (
      <TableHeaderFilterButton
        label="First Name"
        order={filter.field === 'firstName' ? filter.order : null}
        onClick={this._handleFilterChange.bind(this, 'firstName')} />
    );
    const lastNameHeader = (
      <TableHeaderFilterButton
        label="Last Name"
        order={filter.field === 'lastName' ? filter.order : null}
        onClick={this._handleFilterChange.bind(this, 'lastName')} />
    );
    const ageHeader = (
      <TableHeaderFilterButton
        label="Age"
        order={filter.field === 'age' ? filter.order : null}
        onClick={this._handleFilterChange.bind(this, 'age')} />
    );
    const tableHeaders = ['#', firstNameHeader, lastNameHeader, ageHeader, 'Actions'];
    let employeesPrepared = employees;
    if (filter) {
      switch (filter.field) {
        case 'firstName':
          employeesPrepared = _.orderBy(employeesPrepared, 'name.first', filter.order);
          break;
        case 'lastName':
          employeesPrepared = _.orderBy(employeesPrepared, 'name.last', filter.order);
          break;
        default:
          employeesPrepared = _.orderBy(employeesPrepared, filter.field, filter.order);
          break;
      }
    }
    let tableBody = [];
    _.map(employeesPrepared, (employee, index) => {
      const firstName = (
        <TableButton
          disabled={isPending}
          onClick={go.toEmployeeDetails(employee.guid).exec}>
          {employee && employee.name ? employee.name.first : ''}
        </TableButton>
      );
      const lastName = (
        <TableButton
          disabled={isPending}
          onClick={go.toEmployeeDetails(employee.guid).exec}>
          {employee && employee.name ? employee.name.last : ''}
        </TableButton>
      );
      const employeeActions = (
        <span>
          <TableButton
            disabled={isPending}
            onClick={go.toEmployeeDetails(employee.guid).exec}>
            edit
          </TableButton>
          <TableButton
            disabled={isPending}
            onClick={this._handleDeleteEmployee.bind(this, employee.guid)}>
            delete
          </TableButton>
        </span>
      );
      tableBody.push([index + 1, firstName, lastName, employee.age, employeeActions]);
    });
    return children
    ? (
      <div>
        {React.Children.map(children, (child) => {
          return React.cloneElement(child, childProps);
        })}
      </div>
    )
    : (
      <div>
        <PendingIndicator isPending={isPending} />
        <PageTitle title="Список сотрудников" />
        <EditableTable
          tableConfig={{
            headers: tableHeaders,
            body: tableBody,
          }} />
        <Button
          disabled={isPending}
          onClick={go.toEmployeeCreate().exec}>
          Создать
        </Button>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const { employees } = state.toJS();
  return { employees };
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    init: () => {
      dispatch(getEmployees());
    },
    employee: {
      getEmployee: (guid) => { dispatch(getEmployee(guid)); },
      create: (employeeData) => { dispatch(createEmployee(employeeData)); },
      update: (employeeData) => { dispatch(updateEmployee(employeeData)); },
      delete: (employeeId) => { dispatch(deleteEmployee(employeeId)); },
    },
  };
}

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { employees } = stateProps;
  const {
    init,
    employee,
  } = dispatchProps;
  const isPending = employees.isPending;
  return _.assign({}, ownProps, {
    actions: {
      init,
      employee,
    },
    data: {
      isPending,
      employees: employees.items,
      selectedEmployee: employees.selectedItem,
    },
    error: {
      employees: employees.error,
    },
  });
};

const employeeActionsShape = {
  create: React.PropTypes.func.isRequired,
  update: React.PropTypes.func.isRequired,
  delete: React.PropTypes.func.isRequired,
};

const actionsShape = {
  init: React.PropTypes.func.isRequired,
  employee: React.PropTypes.shape(employeeActionsShape).isRequired,
};

const dataShape = {
  employees: React.PropTypes.array.isRequired,
  selectedEmployee: React.PropTypes.object.isRequired,
};

EmployeesPage.propTypes = {
  actions: React.PropTypes.shape(actionsShape).isRequired,
  data: React.PropTypes.shape(dataShape).isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(EmployeesPage);
