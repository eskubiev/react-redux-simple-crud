/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { go } from '../../routes';

// components
import Jumbotron from 'components/Jumbotron';

class HomePage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div className="container">
        <Jumbotron
          header="Простое CRUD приложение на React Redux"
          leadText="Это полноценное SPA CRUD приложение для просмотра, редактирования и удаления записей сотрудников, созданное при помощи стрека React, React Redux и Redux Saga."
          buttonLabel="Список сотрудников"
          buttonOnClick={go.toEmployeesList().exec} />
        <div className="container">
          <h2>Особенности</h2>
          <h3>React</h3>
          <p><a href="https://facebook.github.io/react/" target="_blank">Современная Javascript библиотека</a> для построения изоморфных приложений, исполняемых на стороне клиента.</p>
          <p>Применяются различные особенности данной библиотеки: повторно используемые компоненты со своими свойствами и состояниями, передача свойств между объектами и т.д. Например, для редактирования существующего и создания нового объекта применяется один и тот же компонент с разным состоянием.</p>
          <h3>Redux</h3>
          <p>В качестве контейнера для глобального состояния приложения используется <a href="http://redux.js.org/" target="_blank">Redux</a> и его реализация для React <a href="https://github.com/reactjs/react-redux" target="_blank">React Redux</a>.</p>
          <h3>Redux Saga</h3>
          <p>Для отдельного потока операций (например, для обращений к API) используется библиотека <a href="http://yelouafi.github.io/redux-saga/" target="_blank">Redux Saga</a>.</p>
          <p>Все запросы к API происходят асинхронно и все приложение создано с учетом особенностей обработки и отображения результатов таких запросов.</p>
          <h3>Маршрутизация</h3>
          <p>Вся маршрутизация происходит внутри приложения при помощи библиотеки <a href="https://github.com/ReactTraining/react-router" target="_blank">React Router</a>.</p>
          <h3>API Ready</h3>
          <p>Проект полностью готов к работе с реальными API, достаточно произвести настройку в соотвутствующих функциях внутри sagas контейнеров и заменить вызовы временных функций на актуальные.</p>
          <p>Предполагается следующая структура ответов на запросы:</p>
          <ul>
            <li><code>GET Employees</code> - возвращает массив объектов</li>
            <li><code>GET Employee</code> - возвращает один объект по ID</li>
            <li><code>CREATE Employee</code> - возвращает созданный объект</li>
            <li><code>UPDATE Employee</code> - возвращает измененный объект</li>
            <li><code>DELETE Employee</code> - возвращает удаленный объект</li>
          </ul>
          <p>На данный момент приложение имитирует ответы и задержку со стороны backend и обрабатывает свое состояние соответствующим образом. После обновления страницы глобальное состояние приложения создается заново, следовательно, все изменения теряются.</p>
          <h3>Валидация</h3>
          <p>Для валидации вводимых данных на стороне клиента используется библиотека <a href="https://validatejs.org/" target="_blank">Validate.js</a>.</p>
          <h3>Фильтры</h3>
          <p>Поддерживается изменение порядка отображения сотрудников в таблице по различным полям по возрастающей и убывающей.</p>
          <h3>Bootstrap</h3>
          <p>В качестве визуальной основы используется Twitter Bottstrap, подключенный через CDN. Из данного фреймворка взяты только CSS стили, вся функциональность написана на React.</p>
          <h2>Начало работы</h2>
          <ol>
            <li>Клонировать этот репозиторий <code>git clone --depth=1 https://eskubiev@bitbucket.org/eskubiev/react-redux-simple-crud.git</code></li>
            <li>Выполнить <code>npm install</code> для установки всех зависимостей пректа</li>
            <li>Выполнить <code>npm start</code></li>
            <li>Проект доступен в браузере по адресу <code>http://localhost:3000</code></li>
          </ol>
          <h2>Документация</h2>
          <p>В качестве основы используется стартовый проект <a href="http://reactboilerplate.com/" target="_blank">React Boilerplate</a> со всеми его особенностями.</p>
        </div>
      </div>
    );
  }
}

export default HomePage;
