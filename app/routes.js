// These are the pages you can go to.
// They are all wrapped in the App component, which should contain the navbar etc
// See http://blog.mxstbr.com/2016/01/react-apps-with-pages for more information
// about the code splitting business
import { getAsyncInjectors } from 'utils/asyncInjectors';
import { browserHistory } from 'react-router';

const errorLoading = (err) => {
  console.error('Dynamic page loading failed', err); // eslint-disable-line no-console
};

const loadModule = (cb) => (componentModule) => {
  cb(null, componentModule.default);
};

export default function createRoutes(store) {
  // Create reusable async injectors using getAsyncInjectors factory
  const { injectReducer, injectSagas } = getAsyncInjectors(store); // eslint-disable-line no-unused-vars

  return [
    {
      path: '/',
      name: 'home',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          System.import('containers/HomePage'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([component]) => {
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: '/employees',
      name: 'employees',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          System.import('containers/employees/EmployeesPage/reducer'),
          System.import('containers/employees/EmployeesPage/sagas'),
          System.import('containers/employees/EmployeesPage'),
        ]);
        const renderRoute = loadModule(cb);
        importModules.then(([reducer, sagas, component]) => {
          injectReducer('employees', reducer.default);
          injectSagas(sagas.default);
          renderRoute(component);
        });
        importModules.catch(errorLoading);
      },
      childRoutes: [
        {
          path: '/employees/details/:employeeId',
          name: 'employeeDetails',
          getComponent(nextState, cb) {
            const importModules = Promise.all([
              System.import('containers/employees/EmployeeDetails'),
            ]);
            const renderRoute = loadModule(cb);
            importModules.then(([component]) => {
              renderRoute(component);
            });
            importModules.catch(errorLoading);
          },
        }, {
          path: '/employees/create',
          name: 'employeeCreate',
          getComponent(nextState, cb) {
            const importModules = Promise.all([
              System.import('containers/employees/EmployeeCreate'),
            ]);
            const renderRoute = loadModule(cb);
            importModules.then(([component]) => {
              renderRoute(component);
            });
            importModules.catch(errorLoading);
          },
        },
      ],
    }, {
      path: '*',
      name: 'notfound',
      getComponent(nextState, cb) {
        System.import('containers/NotFoundPage')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    },
  ];
}

export const go = {
  toHome: () => ({
    exec: () => { browserHistory.push('/'); },
  }),
  toEmployeesList: () => ({
    exec: () => { browserHistory.push('/employees'); },
  }),
  toEmployeeDetails: (employeeId) => ({
    exec: () => { browserHistory.push(`/employees/details/${employeeId}`); },
  }),
  toEmployeeCreate: () => ({
    exec: () => { browserHistory.push('/employees/create'); },
  }),
};
